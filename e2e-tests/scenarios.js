'use strict';

describe('Angular Search UI Application', function() {


  it('should redirect `index.html` to `index.html#!/results', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toBe('/results');
  });

  describe('View: resultList', function() {

    beforeEach(function() {
      browser.get('index.html#/results');
    });

    it('should filter the result list as a user types into the search box', function() {
      var resultList = element.all(by.repeater('result in $ctrl.results'));
      var query = element(by.model('$ctrl.query'));

      expect(resultList.count()).toBe(10);

      query.sendKeys('syndrome');
      expect(resultList.count()).toBe(1);

      query.clear();
      query.sendKeys('misuse');
      expect(resultList.count()).toBe(4);
    });

   it('should be possible to control result order via the drop-down menu', function() {
      var queryField = element(by.model('$ctrl.query'));
      var orderSelect = element(by.model('$ctrl.orderProp'));
      var titleOption = orderSelect.element(by.css('option[value="title"]'));
      var resultTitleColumn = element.all(by.repeater('result in $ctrl.results').column('result.fields.TITLE'));

      function getTitles() {
        return resultTitleColumn.map(function(elem) {
          return elem.getText();
        });
      }

      queryField.sendKeys('misuse');   // Let's narrow the dataset to make the assertions shorter

      expect(getTitles()).toEqual([
        'Alcohol misuse - NHS Choices',
        'Alcohol misuse - NHS Choices',
        'Alcohol misuse - Risks - NHS Choices',
        'Alcohol misuse - Treatment - NHS Choices'
      ]);

      /*titleOption.click();

      expect(getTitles()).toEqual([
        'Diarrhoea and vomiting (gastroenteritis)',
        'Norovirus'
      ]);*/

      /*
      element.all(by.css('.results li a')).first().click();
        expect(browser.getLocationAbsUrl()).toBe('http://www.nhs.uk/conditions/Alcohol-misuse/Pages/Introduction.aspx');
      */

    });

  });

   describe('View: Result detail', function() {

      beforeEach(function() {
        browser.get('index.html#!/results/123');
      });

      it('should display placeholder page with `resultId`', function() {
        expect(element(by.binding('$ctrl.resultId')).getText()).toBe('123');
      });

    });

});