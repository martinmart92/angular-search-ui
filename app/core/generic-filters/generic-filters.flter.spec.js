'use strict';

describe('testFilter', function() {

  beforeEach(module('core'));

  it('should convert in values to out',
    inject(function(testFilterFilter) {
      expect(testFilterFilter('in')).toBe('out');
    })
  );

});