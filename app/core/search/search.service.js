angular.
  module('core.search').
  factory('Search', ['$resource',
    function($resource) {
      return $resource('results/:key.json', {}, {
        query: {
          method: 'GET',
          params: {key: 'results'},
          isArray: false
        },
         simpleSearch: {
             url: 'http://localhost:8080/search-simple?input=:input',
             method: 'GET',
             isArray: false
           },
        naturalSearch: {
            url: 'http://localhost:8080/search-natural?input=:input',
            method: 'GET',
            isArray: false
          }
      });
    }
  ]);