'use strict';

angular.
  module('searchUiApp').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/results', {
          template: '<result-list></result-list>'
        }).
        when('/results/:resultId', {
          template: '<result-detail></result-detail>'
        }).
        otherwise('/results');
    }
  ]);