// Define the `searchUiApp` module
angular.module('searchUiApp', [
                                // dependencies to sub modules
                                'ngAnimate',
                                'ngRoute',
                                'core',
                                'resultDetail',
                                'resultList',
]);