angular.
  module('resultList').
  component('resultList', {
    templateUrl: 'result-list/result-list.template.html',
    controller: ['Search', function ResultListController(Search) {
          var self = this;
          self.numberOfResults = null;
          this.search = function() {
            console.log("ResultListController.search() : with input : "+self.query);

            Search.naturalSearch({input: self.query}, function(response) {
                console.log("ResultListController.naturalSearch() result has response : "+response);
                self.results = response.results;
                self.numberOfResults = response.numberOfResults;
            });

          }
        }
    ]
  });