'use strict';

describe('resultList', function() {

  // Load the module that contains the `searchUiApp` component before each test
  beforeEach(module('resultList'));

  // Test the controller
  describe('SearchResultsListController', function() {

    var $httpBackend, ctrl;

    beforeEach(inject(function($componentController, _$httpBackend_) {

        $httpBackend = _$httpBackend_;
        $httpBackend.expectGET('results/results.json')
                      .respond({
                                  "numberOfResults": 10,
                                  "results": [
                                    {
                                      "fields": {
                                        "CONCEPT_NAME": "Alcohol misuse ",
                                        "TITLE": "Alcohol misuse - NHS Choices",
                                        "URL": "http://www.nhs.uk/conditions/Alcohol-misuse/Pages/Introduction.aspx"
                                      }
                                    },
                                    {
                                      "fields": {
                                        "CONCEPT_NAME": "Alcohol-related liver disease ",
                                        "TITLE": "Alcohol-related liver disease - NHS Choices",
                                        "URL": "http://www.nhs.uk/conditions/Liver_disease_(alcoholic)/Pages/Introduction.aspx"
                                      }
                                    }
                                  ]
                                });

      ctrl = $componentController('resultList');
    }));

    it('should create a `results` property with 2 results fetched with `$http`', function() {
      expect(ctrl.results).toBeUndefined();


      $httpBackend.flush();
      expect(ctrl.results).toEqual([
                                   {
                                     "fields": {
                                       "CONCEPT_NAME": "Alcohol misuse ",
                                       "TITLE": "Alcohol misuse - NHS Choices",
                                       "URL": "http://www.nhs.uk/conditions/Alcohol-misuse/Pages/Introduction.aspx"
                                     }
                                   },
                                   {
                                     "fields": {
                                       "CONCEPT_NAME": "Alcohol-related liver disease ",
                                       "TITLE": "Alcohol-related liver disease - NHS Choices",
                                       "URL": "http://www.nhs.uk/conditions/Liver_disease_(alcoholic)/Pages/Introduction.aspx"
                                     }
                                   }
                                 ]);
    });

    it('should set a default value for the `orderProp` property', function() {
          expect(ctrl.orderProp).toBe('age');
    });

  });

});