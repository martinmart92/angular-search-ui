'use strict';

angular.
  module('resultDetail').
  component('resultDetail', {
    template: 'TBD: Detail view for <span>{{$ctrl.resultId}}</span>',
    controller: ['$routeParams',
      function ResultDetailController($routeParams) {
        this.resultId = $routeParams.resultId;
      }
    ]
  });